# Project Title

Descripción de un parrafo


### Prerequisites

Cosas que necesitas instalar en el sistema y como instalarlas

```
ejemplo
```

### Installing

Ejemplos paso a paso para levantar el ambiente de desarrollo

Ejemplo de primer paso

```
ejemplo
```

Ejemplo de primer paso 2

```
hasta el fin
```


## Running the tests

Explicacion de como correr los test automatisados para el proyecto

### Break down into end to end tests

Explicacion de las cosas que se estan testeando y un porque

```
ejemplo
```

### Coding style tests

Explicacion de las cosas que se estan testeando y un porque

```
ejemplo
```

## Deployment || Pull Request

Notas de como hacer deploy  del proyecto o de como crear los pull request



## Built With

* [VueJs](https://vuejs.org/) - El framework usado
* [FireBase](https://firebase.google.com) - Dependencia
* [GitFlow](https://danielkummer.github.io/git-flow-cheatsheet/) - Branching model


## Versioning

Nosotros usamos [SemVer](http://semver.org/) para el versionamiento.
Para las versiones disponibles, ver los [tags de este repositorio](https://github.com/your/project/tags).